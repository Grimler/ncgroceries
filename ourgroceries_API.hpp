/**   
 *    ncgroceries is a CL utility for handling ourgroceries.com shopping lists
 *    Copyright (C) 2019 Henrik Grimler
 *
 *    This file is part of ncgroceries.
 *
 *    ncgroceries is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    ncgroceries is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with ncgroceries. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef OURGROCERIES_API_H
#define OURGROCERIES_API_H

#include "groceries.hpp"

#include <json/json.h>
#include <curl/curl.h>

int doLogin(string, string, bool);
int retrieveTeamId(void *, string*);
int parseList(istringstream *, map<string, info>*);
int getList(string, string, map<string, info>*, bool);
int getTeamId(string *, bool);
int addItem(string, string, string, map<string, info>*, bool);
int setItemCrossedOff(string, string, map<string,info>::iterator,
                      map<string, info>*, bool);
int insertItem(string, string, string, map<string, info>*, bool);
int getOverview(string, map<string, string>*, bool);

#endif /* OURGROCERIES_API_H */
