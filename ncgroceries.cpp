/**
 *    ncgroceries is a CL utility for handling ourgroceries.com shopping lists
 *    Copyright (C) 2019 Henrik Grimler
 *
 *    This file is part of ncgroceries.
 *
 *    ncgroceries is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    ncgroceries is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with ncgroceries. If not, see <https://www.gnu.org/licenses/>.
 */

#include "ourgroceries_API.hpp"
#include <curses.h>
#include <fstream>

int cleanup_and_exit(int exit_code)
{
  curl_global_cleanup();
  exit(exit_code);
}

void print_win_params(WINDOW *p_win)
{
  mvprintw(25, 0, "%d %d %d %d", p_win->_begx, p_win->_begy, 
           p_win->_curx, p_win->_cury);
}

int print_lists(map<string, info> *list, int lin0, int col0, int nlines, int* true_elem) {
  map<string, info>::iterator it;
  int line_true, line_false;

  line_true = 1;
  line_false = 1;

  for (it=(*list).begin(); it != (*list).end(); it++) {
    if (!(it->second.crossedOff)) {
      if (line_true < nlines-1) {
        mvprintw(lin0 + line_true, col0 + 1, "%s", it->first.c_str());
        line_true ++;
      }
    } else {
      if (line_false < nlines/2-1) {
        mvprintw(lin0 + nlines+1 + line_false, col0 + 1, "%s", it->first.c_str());
        line_false ++;
      }
    }
  }
  *true_elem = line_true;

  while (line_true < nlines-1) {
    mvprintw(lin0 + line_true, col0 + 1, "%s", "                    ");
    line_true ++;
  }

  return 0;
}

int main(int argc, char *argv[])
{
  string teamId;
  string email;
  string pass;
  string main_list;
  bool verify_ssl = true;
  map<string, string> listArray;
  map<string, string>::iterator string_it;
  map<string, info> shoppingList;
  map<string, info>::iterator shopping_it;

  setlocale(LC_ALL, "");

  curl_global_init(CURL_GLOBAL_ALL);

  if (initialize() != 0)
    cout << "Seems we ran into an error when initializing ncgroceries" << endl;

  read_config(&email, &pass, &main_list);

  
  if (doLogin(email, pass, verify_ssl) != 0) {
    cout << "An error occured while logging in to ourgroceries" << endl;
    cleanup_and_exit(EXIT_FAILURE);
  }

  if (getTeamId(&teamId, verify_ssl) != 0) {
    cout << "An error occured while getting teamId" << endl;
    cleanup_and_exit(EXIT_FAILURE);
  }

  if (getOverview(teamId, &listArray, verify_ssl) != 0) {
    cout << "An error occured while getting overview" << endl;
    cleanup_and_exit(EXIT_FAILURE);
  }

  if (listArray.count(main_list) == 1) {
    if (getList(teamId, listArray.find(main_list)->second, &shoppingList, verify_ssl) != 0) {
      cout << "An error occured while getting list" << endl;
      cleanup_and_exit(EXIT_FAILURE);
    }
  } else {
    cout << "Error: " << main_list << " does not seem the name of a shopping list in your account" << endl;
    cleanup_and_exit(EXIT_FAILURE);
  }

  int lin0;
  int col0;
  int lin = 1;
  int col = 1;
  int nlines;
  int ncols;
  int h, w;
  int ch;
  int num_true_elem;
  WINDOW *win;
  WINDOW *crossed_win;
  WINDOW *current_win;

  initscr();
  noecho();
  keypad(stdscr, TRUE);
  getmaxyx(stdscr, h, w);

  nlines = 2*h/3;
  ncols = w/2;
  lin0 = 0;
  col0 = w/4;

  win = newwin(nlines, ncols, lin0, col0);
  wborder(win, '|', '|', '-', '-', '+', '+', '+', '+');
  refresh();
  crossed_win = newwin(nlines/2, ncols, lin0+nlines+1, col0);
  wborder(crossed_win, '|', '|', '-', '-', '+', '+', '+', '+');
  refresh();

  current_win = win;

  print_lists(&shoppingList, lin0, col0, nlines, &num_true_elem);
  wrefresh(win);
  wrefresh(crossed_win);

  // std::ofstream log;
  // log.open ("log.txt");

  while((ch = getch()) != 'q')
  {
    refresh();
    switch(ch)
      {
      case KEY_LEFT:
        if (col-1 > 0) {
          col -= 1;
        };
        wmove(current_win, lin, col);
        wrefresh(current_win);
        break;
      case KEY_RIGHT:
        if (col+1 < ncols-1) {
          col += 1;
        };
        wmove(current_win, lin, col);
        wrefresh(current_win);
        break;
      case KEY_UP:
        if (lin-1 == 0 && current_win == crossed_win) {
          /* Move from bottom win to top win */
          current_win = win;
          lin = nlines-2;
        } else if (lin-1 > 0) {
          lin -= 1;
        };
        wmove(current_win, lin, col);
        wrefresh(current_win);
        break;
      case KEY_DOWN:
        if (lin+1 == nlines-1) {
          /* Move from top win to bottom win */
          current_win = crossed_win;
          lin = 1;
        } else if (lin+1 < nlines-1 && current_win == win) {
          lin += 1;
        } else if (lin+1 < nlines/2-1 && current_win == crossed_win) {
          lin += 1;
        };
        wmove(current_win, lin, col);
        wrefresh(current_win);
        break;
      case 10:
        /* KEY_ENTER: https://stackoverflow.com/a/38214436 */
        // log << lin << " " << num_true_elem << std::endl;
        if (current_win == win && lin < num_true_elem) {
          int true_elem = 0;
          for (shopping_it=shoppingList.begin(); shopping_it!=shoppingList.end(); ++shopping_it) {
            if (shopping_it->second.crossedOff == false) {
              true_elem ++;
            }
            if (true_elem == lin)
              break;
            }
          // log << "Crossing" << " " << shopping_it->first << std::endl;
          if (setItemCrossedOff(listArray.find(main_list)->second, teamId,
                                shopping_it, &shoppingList, verify_ssl) != 0)
            std::cout << "An error occured while crossing item off" << std::endl;
        }
        if (getList(teamId, listArray.find(main_list)->second, &shoppingList, verify_ssl) != 0) {
          cout << "An error occured while getting list" << endl;
          cleanup_and_exit(EXIT_FAILURE);
        }
        print_lists(&shoppingList, lin0, col0, nlines, &num_true_elem);
        wrefresh(win);
        wrefresh(crossed_win);
        break;
      case 'r':
        refresh();
        break;
      case 'w':
        wrefresh(win);
        wrefresh(crossed_win);
        break;
      default:
        break;
      }
    // log << current_win->_begx << " " << current_win->_begy << " " << current_win->_curx << " " << current_win->_cury << std::endl;
  }
  // log.close();
  endwin();
  curl_global_cleanup();
  return 0;
}
