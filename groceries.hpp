/**   
 *    ncgroceries is a CL utility for handling ourgroceries.com shopping lists
 *    Copyright (C) 2019 Henrik Grimler
 *
 *    This file is part of ncgroceries.
 *
 *    ncgroceries is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    ncgroceries is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with ncgroceries. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NCGROCERIES_H
#define NCGROCERIES_H

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <sys/stat.h>
#include <fstream>
#include <regex>
#include <list>
#include <map>
#include <filesystem>
#include <dirent.h>
#include <errno.h>

#include <popt.h>

using namespace std;

extern string ncgroceries_folder;
extern string cookiefile;
extern string main_list;

struct info {string id; bool crossedOff; string category;};

int initialize();
int read_config(string*, string*, string*);
int doLogin(string, string, bool);
//static size_t retreiveTeamIdWrapper(void, size_t, size_t, void);
//int parse_arguments(int, const char*, bool*, string*, string*, string*)
int getList(string, string, map<string, info>*, bool);
int retreiveTeamId(void*, string*);
int getTeamId(string*, bool);
int getOverview(string, map<string, string>*, bool);
int insertItem(string, string, string, map<string, info>*, bool);
int addItem(string, string, string, map<string, info>*, bool);
int setItemCrossedOff(string, string, string, map<string, info>*, bool);

#endif
