/**
 *    ncgroceries is a CL utility for handling ourgroceries.com shopping lists
 *    Copyright (C) 2019 Henrik Grimler
 *
 *    This file is part of ncgroceries.
 *
 *    ncgroceries is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    ncgroceries is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with ncgroceries. If not, see <https://www.gnu.org/licenses/>.
 */

#include <libconfig.h++>

#include "groceries.hpp"

using namespace libconfig;

/* https://stackoverflow.com/a/12774387 */
inline bool file_exists_test (const string& name) {
    ifstream f(name.c_str());
    return f.good();
}

/* https://stackoverflow.com/a/236803 */
template<typename Out>
void split(const string &s, char delim, Out result) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
      *(result++) = item;
    }
}

vector<string> split(const string &s, char delim) {
    vector<string> elems;
    split(s, delim, back_inserter(elems));
    return elems;
}

int read_config(string *email, string *password, string *main_list)
{
  Config cfg;
  string conffile;

  // Read the file. If there is an error, report it and exit.
  if (file_exists_test(ncgroceries_folder + "groceries.conf")) {
    conffile = ncgroceries_folder + "groceries.conf";
  } else {
    conffile = "groceries.conf";
  }
  try
  {
    cfg.readFile(conffile.c_str());
  }
  catch(const FileIOException &fioex)
  {
    cout << "No config file found" << endl;
    return(EXIT_FAILURE);
  }
  catch(const ParseException &pex)
  {
    cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine()
              << " - " << pex.getError() << endl;
    return(EXIT_FAILURE);
  }

  // Get email.
  try
  {
    *email = cfg.lookup("email").c_str();
  }
  catch(const SettingNotFoundException &nfex)
  {
    cout << "No 'email' setting in configuration file." << endl;
  }

  // Get password.
  try
  {
    *password = cfg.lookup("password").c_str();
  }
  catch(const SettingNotFoundException &nfex)
  {
    cout << "No 'password' setting in configuration file." << endl;
  }

  // Get name of main shopping list.
  try
  {
    *main_list = cfg.lookup("main_list").c_str();
  }
  catch(const SettingNotFoundException &nfex)
  {
    cout << "No 'main_list' setting in configuration file." << endl;
  }
  return 0;
}

int initialize()
{
  DIR* dir = opendir(ncgroceries_folder.c_str());
  if (dir)
  {
    /* Directory exists. */
    closedir(dir);
    return 0;
  }
  else if (ENOENT == errno)
  {
    /* Directory does not exist. */
    mkdir(ncgroceries_folder.c_str(), 0777);
    return 0;
  }
  else
  {
    /* opendir() failed for some other reason. */
    cerr << "Unknown error has occured when creating folder" << endl;
    return errno;
  }
}
