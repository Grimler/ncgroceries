/**
 *    ncgroceries is a CL utility for handling ourgroceries.com shopping lists
 *    Copyright (C) 2019 Henrik Grimler
 *
 *    This file is part of ncgroceries.
 *
 *    ncgroceries is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    ncgroceries is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with ncgroceries. If not, see <https://www.gnu.org/licenses/>.
 */

#include "ourgroceries_API.hpp"

char login[]     = "https://www.ourgroceries.com/sign-in";
char yourlists[] = "https://www.ourgroceries.com/your-lists/";

extern string ncgroceries_folder;
extern string cookiefile;

string home (getenv("HOME"));
string ncgroceries_folder (home + "/.ncgroceries/");
string cookiefile (ncgroceries_folder + "cookies.txt");

struct MemoryStruct {
  char *memory;
  size_t size;
};

/* Default curl callback function from https://curl.haxx.se/libcurl/c/getinmemory.html
 * Used to gather html response, instead of printing to user
 */
static size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct MemoryStruct *mem = (struct MemoryStruct *)userp;

  char *ptr = (char*)realloc(mem->memory, mem->size + realsize + 1);
  if(ptr == NULL) {
    /* out of memory! */
    printf("not enough memory (realloc returned NULL)\n");
    return 0;
  }

  mem->memory = ptr;
  memcpy(&(mem->memory[mem->size]), contents, realsize);
  mem->size += realsize;
  mem->memory[mem->size] = 0;

  return realsize;
}

int doLogin(string username, string password, bool verify_ssl)
{
  CURL *curl = nullptr;
  CURLcode res;
  char *encoded_pw = curl_easy_escape(curl, password.c_str(), strlen(password.c_str()));
  string data ("emailAddress="+username+"&password="+encoded_pw+"&action=sign-in");
  long response_code;
  struct MemoryStruct chunk;

  curl_free(encoded_pw);
  chunk.memory = (char*)malloc(1);
  chunk.size = 0;

  curl = curl_easy_init();

  curl_easy_setopt(curl, CURLOPT_URL, login);

  if (!verify_ssl) {
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
  }

  curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.c_str());
  curl_easy_setopt(curl, CURLOPT_COOKIEJAR, cookiefile.c_str());

  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

  if (curl) {
    /* Perform the request, res will get the return code */
    res = curl_easy_perform(curl);

    if (res != CURLE_OK) {
      cerr << "curl_easy_perform() failed: " << curl_easy_strerror(res) << endl;
      curl_easy_cleanup(curl);
      return res;
    }
  }

  curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
  /* 302 is returned on success.
   * 200 and a "sign up now"-page is typically returned on login error
   */
  if (response_code != 302) {
    cerr << "Login failed, check your username and password" << endl;
    curl_easy_cleanup(curl);
    return EXIT_FAILURE;
  }

  curl_easy_cleanup(curl);
  return 0;
}

int retrieveTeamId(void *response, string* teamId)
{
  regex teamIdRegex (".*var g_teamId = \"(\\w+)\";.*");
  smatch match;
  char *contents = (char*) response;
  string resp (contents);

  /* Skip obtaining shoppingLists here for now
  regex jsonShoppingListRegex (".*var g_shoppingLists = \\[\n(.*)\\];.*");
  regex shoppingListRegex (".*\\{ id: \"(.*)\", name: \"(.*)\".*");
  vector<string> jsonShoppingListArray;
  map<string,string> shoppingListMap;
  if (regex_search(resp, match, jsonShoppingListRegex)) {
    jsonShoppingListArray = split(match[1], '}');
  }

  for(vector<string>::iterator it = jsonShoppingListArray.begin(); it != jsonShoppingListArray.end(); ++it) {
    if (regex_search(*it, match, shoppingListRegex)) {
        shoppingListMap.insert( pair<string,string>(match[2], match[1]) );
    }
  }

  cout << "Shopping Lists: ";
  for (map<string,string>::iterator it = shoppingListMap.begin(); it != shoppingListMap.end(); ++it) {
    cout << it->first << " ";
  }
  cout << endl;
  */

  if (regex_search(resp, match, teamIdRegex)) {
    *teamId = match[1];
  }
  return 0;
}

static size_t retrieveTeamIdWrapper(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  string *teamId = (string*)userp;

  retrieveTeamId(contents, teamId);

  return realsize;
}

static size_t parseList_callback(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  istringstream str((char*) contents);
  map<string, info>* shoppingList = (map<string, info>*) userp;

  if (parseList(&str, shoppingList) != 0)
    return -1;

  return realsize;
}

int parseList(istringstream *json_string, map<string, info>* shoppingList) {
  Json::CharReaderBuilder builder;
  Json::Value root;

  bool parsingSuccessful;
  string errs;
  int index;

  parsingSuccessful = Json::parseFromStream( builder, *json_string, &root, &errs );
  if ( !parsingSuccessful ) {
    // report to the user the failure and their locations in the document.
    cout << errs << endl;
    return 1;
  }

  /* Need to check if values are crossedOff or not */
  const Json::Value json_list = root["list"]["items"];
  for ( index = 0; index < (int)json_list.size(); ++index ) {
    (*shoppingList).insert( pair<string,info>( json_list[index].get("value", "ERROR").asString(),
                                               { json_list[index].get("id", "ERROR").asString(),
                                                 json_list[index].get("crossedOffAt", false) != false,
                                                 json_list[index].get("categoryId", "empty").asString()}));
  }
  return 0;
}

int getList(string teamId, string listName, map<string, info>* shoppingList, bool verify_ssl)
{
  CURL *curl;
  CURLcode res;
  struct curl_slist *chunk = NULL;

  /* get a curl handle */
  curl = curl_easy_init();

  chunk = curl_slist_append(chunk, "Content-type: application/json");

  curl_easy_setopt(curl, CURLOPT_URL, yourlists);
  curl_easy_setopt(curl, CURLOPT_COOKIEFILE, cookiefile.c_str());
  curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, parseList_callback);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)shoppingList);

  string command ("{\"command\" : \"getList\", \"listId\" : \""+listName+"\", \"teamId\" : \""+teamId+"\", \"versionId\" : \"\"}");

  curl_easy_setopt(curl, CURLOPT_POSTFIELDS, command.c_str());

  if (! verify_ssl)
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);

  /* Perform the request, res will get the return code */
  res = curl_easy_perform(curl);
  curl_slist_free_all(chunk);

  if(res != CURLE_OK) {
    fprintf(stderr, "curl_easy_perform() failed: %s\n",
            curl_easy_strerror(res));
  }
  curl_easy_cleanup(curl);
  return 0;
}

int getTeamId(string *teamId, bool verify_ssl)
{
  CURL *curl;
  CURLcode res;

  curl = curl_easy_init();

  curl_easy_setopt(curl, CURLOPT_URL, yourlists);
  /**
   * Getting gzip data reduces traffic to less than half, but I then have to decompress it (using boost?). 
   * Skip for now
   * curl_easy_setopt(curl, CURLOPT_HTTPHEADER, curl_slist_append(chunk, "Accept-Encoding: gzip, deflate, br");
   */
  if (! verify_ssl)
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);

  curl_easy_setopt(curl, CURLOPT_COOKIEFILE, cookiefile.c_str());

  /* send all data to this function */
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, retrieveTeamIdWrapper);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)teamId);
  if (curl) {
    res = curl_easy_perform(curl);
    if(res != CURLE_OK) {
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
      return res;
    }
  }
  curl_easy_cleanup(curl);
  return 0;
}

int addItem(string listId, string teamId, string itemToAdd, map<string, info>* shoppingList, bool verify_ssl)
{
  /* We should check if item already exists in list and in that case use changeItemValue instead of insertItem
   * ourgroceries seem to accept insertItem (calling it increments existing item), so let's wait with implementing
   * changeItemValue.
   */
  // if (it != (*shoppingList).end()) {
  //   // Item already exists in list so we want to increment list value instead
  //   string value = (itemToAdd + "(2)");
  //   changeItemValue(listId, teamId, itemToAdd, shoppingList, verify_ssl)
  // } else {
  insertItem(listId, teamId, itemToAdd, shoppingList, verify_ssl);
  // }
  return 0;
}

int setItemCrossedOff(string listId, string teamId, map<string,info>::iterator item, map<string, info>* shoppingList, bool verify_ssl)
{
  CURL *curl;
  CURLcode res;
  string itemId;
  struct curl_slist *chunk = NULL;
  itemId = item->second.id;
  string setItemCrossedOff ("{\"command\" : \"setItemCrossedOff\", \"crossedOff\" : true, \"itemId\" : \""+itemId+"\", \"listId\" : \""+listId+"\", \"teamId\" : \""+teamId+"\"}");

  /* get a curl handle */
  curl = curl_easy_init();
  chunk = curl_slist_append(chunk, "Content-type: application/json");

  curl_easy_setopt(curl, CURLOPT_URL, yourlists);
  curl_easy_setopt(curl, CURLOPT_COOKIEFILE, cookiefile.c_str());
  curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
  curl_easy_setopt(curl, CURLOPT_POSTFIELDS, setItemCrossedOff.c_str());

  if (! verify_ssl)
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);

  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, parseList_callback);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)shoppingList);

  /* Delete item from list, as it would otherwise stick around locally */
  (*shoppingList).erase(item);

  /* Perform the request, res will get the return code */
  res = curl_easy_perform(curl);

  if(res != CURLE_OK) {
    fprintf(stderr, "curl_easy_perform() failed: %s\n",
            curl_easy_strerror(res));
    return res;
  }
  curl_easy_cleanup(curl);
  curl_global_cleanup();

  return 0;
}

int insertItem(string listId, string teamId, string itemToAdd, map<string, info>* shoppingList, bool verify_ssl)
{
  CURL *curl;
  CURLcode res;
  struct curl_slist *chunk = NULL;
  map<string,info>::iterator it;

  string insertItem ("{\"command\" : \"insertItem\", \"listId\" : \""+listId+"\", \"teamId\" : \""+teamId+"\", \"value\" : \""+itemToAdd+"\"}");

  /* get a curl handle */
  curl = curl_easy_init();
  chunk = curl_slist_append(chunk, "Content-type: application/json");

  curl_easy_setopt(curl, CURLOPT_URL, yourlists);
  curl_easy_setopt(curl, CURLOPT_COOKIEFILE, cookiefile.c_str());
  curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
  curl_easy_setopt(curl, CURLOPT_POSTFIELDS, insertItem.c_str());

  if (! verify_ssl)
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);

  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, parseList_callback);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)shoppingList);

  /* Perform the request, res will get the return code */
  res = curl_easy_perform(curl);

  // for (map<string, string>::iterator it=(*listArray).begin(); it!=(*listArray).end(); ++it)
  //   cout << it->first << " => " << it->second << endl;

  if(res != CURLE_OK) {
    fprintf(stderr, "curl_easy_perform() failed: %s\n",
            curl_easy_strerror(res));
    return res;
  }
  curl_easy_cleanup(curl);

  return 0;
}

static size_t getListsFromOverview(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  istringstream str((char*) contents);
  map<string, string>* listArray = (map<string, string>*) userp;
  string errs;

  Json::Value root;
  Json::CharReaderBuilder builder;

  bool parsingSuccessful = Json::parseFromStream( builder, str, &root, &errs );
  if ( !parsingSuccessful )
    {
      // report to the user the failure and their locations in the document.
      cout << errs << endl;
    }

  const Json::Value shoppingLists = root["shoppingLists"];

  for ( int index = 0; index < (int)shoppingLists.size(); ++index )
    {
      (*listArray).insert( pair<string,string>( shoppingLists[index].get("name", "ERROR").asString(),
                                                shoppingLists[index].get("id", "ERROR").asString() ) );
    }

  /*
  const Json::Value receipes = root["receipes"];
  for ( int index = 0; index < (int)receipes.size(); ++index )
    {
      (*listArray).insert( pair<string,string>( receipes[index].get("name", "ERROR").asString(),
                                                receipes[index].get("id", "ERROR").asString() ) );
    }
  */
  // for (map<string, string>::iterator it=(*listArray).begin(); it!=(*listArray).end(); ++it)
  //   cout << it->first << " => " << it->second << endl;
  return realsize;
}

int getOverview(string teamId, map<string, string>* listArray, bool verify_ssl)
{
  CURL *curl;
  CURLcode res;
  struct curl_slist *chunk = NULL;
  // map<string,string> listArray;
  map<string,string>::iterator it;

  string Overview ("{\"command\" : \"getOverview\", \"teamId\" : \""+teamId+"\"}");

  /* get a curl handle */
  curl = curl_easy_init();
  chunk = curl_slist_append(chunk, "Content-type: application/json");

  curl_easy_setopt(curl, CURLOPT_URL, yourlists);
  curl_easy_setopt(curl, CURLOPT_COOKIEFILE, cookiefile.c_str());
  curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
  curl_easy_setopt(curl, CURLOPT_POSTFIELDS, Overview.c_str());

  if (! verify_ssl)
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);

  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, getListsFromOverview);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)listArray);

  /* Perform the request, res will get the return code */
  res = curl_easy_perform(curl);
  curl_slist_free_all(chunk);

  if(res != CURLE_OK) {
    fprintf(stderr, "curl_easy_perform() failed: %s\n",
            curl_easy_strerror(res));
    return res;
  }
  curl_easy_cleanup(curl);
  return 0;
}
