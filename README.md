# ncgroceries

ncgroceries is a ncurses client for
[ourgroceries.com](https://ourgroceries.com).  It is in no way an
official client and the author is in no way affiliated with
[ourgroceries.com](https://ourgroceries.com).  ncgroceries is written
in c++ and also provides a CLI program groceries-cli.  API functions
are built into a library called libgroceries.

Note: this is a work in progress, the ncurses interface is not
finished.  The TUI right now shows an unsorted shopping list where
things can be crossed off but not added.  The CLI (groceries-cli) works
pretty well though.

The code was inspired by
[our-groceries-client](https://github.com/sethwebster/our-groceries-client)
(written in nodejs).  [Mitmproxy](https://mitmproxy.org/) was used to
study the traffic between the official ourgroceries client and their
servers.  Another related project providing the API functions, written
in python, is
[py-our-groceries](https://github.com/ljmerza/py-our-groceries).

## Running groceries-cli

The accepted arguments are (as seen by `groceries-cli --help`

```
Usage: groceries-cli [OPTION...]
  -v, --verify-ssl[=yes/no]     Verify SSL traffic (no allows for MITM inspection)
  -u, --username[=email]        Email address for logging into Our Groceries
  -p, --password[=password]     Password for logging into Our Groceries
  -l, --list[=list-name]        Name of shopping list to retrieve
  -a, --add[=item]              Item to add
  -x, --cross[=item]            Item to cross off
  -L, --print-list              print content of main list

Help options:
  -?, --help                    Show this help message
      --usage                   Display brief usage message
```

Username, password and list can also be specified in a config file at
`~/.ncgroceries/groceries.conf`. An example conf file is provided that
can be copied to that location and then edited.  Multiple items to add
or cross can be given as a comma separated list on the format: `-a
"[item1, item2, item with space]"`.

## Building from source

ncgroceries and groceries-cli depend on libcurl, libconfig, libpopt,
jsoncpp and ncurses. It uses cmake for handling the configuring and
making.

To install necessary dependencies and build tools, you can run (as
root):

On ubuntu:
```sh
apt update && apt install libcurl libcurl-dev libconfig libconfig-dev libpopt libpopt-dev jsoncpp jsoncpp-dev ncurses ncurses-dev cmake build-essential
```

On archlinux:
```sh
pacman -Syu libcurl libconfig libpopt jsoncpp ncurses cmake gcc
```

After getting the dependencies you can do an out of source build by running
```sh
mkdir build && cd build
cmake ..
make
```
