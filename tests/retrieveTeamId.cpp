/**
 *    ncgroceries is a CL utility for handling ourgroceries.com shopping lists
 *    Copyright (C) 2019 Henrik Grimler
 *
 *    This file is part of ncgroceries.
 *
 *    ncgroceries is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    ncgroceries is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with ncgroceries. If not, see <https://www.gnu.org/licenses/>.
 */

#include "ourgroceries_API.hpp"

int main()
{
  const char *response = ""
  ""
  ""
  ""
  "<!DOCTYPE HTML>"
  "<html>"
  "    <head>"
  "        <title>OurGroceries - Your Lists</title>"
  "        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"
  "        <!-- Tell IE to not go into Compatibility View. Also enable Chrome Frame. -->"
  "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">"
  "        <!-- Leave this here for webmaster tools verification: -->"
  "        <meta name=\"google-site-verification\" content=\"GRb_PrELkNrtehNqX_eO8gtL4kRrtpGhazG8oEWkTJO\">"
  "        <meta name=\"viewport\" content=\"width=770\">"
  "        <link rel=\"stylesheet\" type=\"text/css\" href=\"/static/jquery-ui/css/custom-theme/jquery-ui-1.8.custom.css?v=16c86r3p04\">"
  "        <link rel=\"stylesheet\" type=\"text/css\" href=\"/static/stylesheets/your-lists.css?v=2o5ot4055y\">"
  "        <link rel=\"stylesheet\" type=\"text/css\" href=\"/static/stylesheets/main.css?v=o23yt451ye\">"
  "        <script type=\"text/javascript\" src=\"/static/jquery-ui/js/jquery-1.4.2.min.js?v=32oktt5t3n\"></script>"
  "        <script type=\"text/javascript\" src=\"/static/jquery-ui/js/jquery-ui-1.8.custom.js?v=tr346521o05\"></script>"
  "        <script type=\"text/javascript\" src=\"/static/js/json.js?v=1e34b4g3y4\"></script>"
  "        <script type=\"text/javascript\" src=\"/static/js/common.js?v=k4tn32ki5g\"></script>"
  "        <script type=\"text/javascript\" src=\"/static/js/your-lists.js?v=45hg365rq2\"></script>"
  "        <link rel=\"shortcut icon\" href=\"/static/images/favicon.ico?v=69r432te43\">"
  "        <script>"
  "          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){"
  "          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),"
  "          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)"
  "          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');"
  "          ga('create', 'UA-575657-2', 'auto');"
  "          ga('send', 'pageview');"
  "        </script>"
  "    </head>"
  "    <body>"
  "            <div class=\"main-frame\">"
  "                <div class=\"main-pane\">"
  "                    <div id=\"main-header-image\"><a href=\"/overview\" title=\"OurGroceries\"><img class=\"header-image\" src=\"/static/images/header.png?v=54r3957540\" srcset=\"/static/images/header@2.png?v=poi54tr3w4 2x\" alt=\"OurGroceries\" width=\"750\" height=\"211\"></a><span id=\"auth-info\"><span id=\"auth-text\"><b>aaaaaa@bbbbbb.com</b> | <a href=\"/settings\">Settings</a> | <a href=\"/sign-out\">Sign out</a></span></span></div>"
  "                    <table id=\"tab-table\" cellpadding=\"0\" cellspacing=\"0\">"
  "                        <tr id=\"tab-labels\">"
  "	                            <td id='overview-label' ><a href='/overview'>Overview</a></td>"
  "    	                            <td id='user-guide-label' ><a href='/user-guide'>User Guide</a></td>"
  "    	                            <td id='faq-label' ><a href='/faq'>FAQ</a></td>"
  "    	                            <td id='your-lists-label' class='current-tab'><a href='/your-lists'>Your Lists</a></td>"
  "    	                            <td id='download-label' ><a href='/download'>Download</a></td>"
  "    	                        </tr>"
  "                    </table>"
  ""
  "<div id=\"your-lists\" class=\"single-column jQueryContainer\">"
  "    <div class=\"listActionButtons\">"
  "    <button id=\"createListButton\" type=\"button\">Add a shopping list</button>"
  "    <button id=\"createRecipeButton\" type=\"button\">Add a recipe</button>"
  "        <a id=\"manageMasterListButton\" href=\"list/cLrAGXz4C3kCSASjXR4jaj\">Manage master list</a>"
  "        </div>"
  ""
  "    <br>"
  "    <br>"
  "    <script type=\"text/javascript\">"
  "        var g_shoppingLists = ["
  "            { id: \"nxM6qAieWd5J0hssbwWPBV\", name: \"default list\" },            { id: \"mb0lqpQEbadQ4kLxANBPaP\", name: \"another list\" }        ];"
  "        var g_recipes = ["
  "            { id: \"oGCuaL4I4a9gPc3p9ctnzU\", name: \"Greek Salad\" }        ];"
  "        var g_categories = ["
  "            { id: \"aalnJKNY7S1Qnnw8sWc0PT\", value: \"Dairy\" },            { id: \"j7qQJwExyzW5O2aTPyLUQA\", value: \"Frozen Food\" },            { id: \"WcxlQ9s0Ki9om0AHQkhRXm\", value: \"Produce\" }        ];"
  "        var g_teamId = \"cvDwQAKokxat770WwtakIU\";"
  "        var g_categoryListId = \"nRL1jKaRJ5Zx6wrG8FwQV1\";"
  "        var g_listType = \"\";"
  "        var g_listName = \"\";"
  "        var g_listId = \"\";"
  "        var g_fromShoppingListId = null;"
  "        var g_addUncategorizedItem = \"LEAVE\";"
  "    </script>"
  ""
  ""
  ""
  "    <div id=\"listDiv\"></div>"
  ""
  "    <div id=\"listNotes\">"
  "        <h2>Notes and Links</h2>"
  "        <div id=\"listNotesView\">"
  "            <div></div>"
  "        </div>"
  "        <div id=\"listNotesEdit\">"
  "            <textarea autocapitalize=\"sentences\" rows=\"10\"></textarea>"
  "            <div id=\"listNotesEditButtons\">"
  "                <button id=\"saveNotesButton\" type=\"button\">Save Notes</button>"
  "                <button id=\"cancelNotesButton\" type=\"button\">Cancel</button>"
  "            </div>"
  "        </div>"
  "    </div>"
  ""
  "    <div id=\"printCredits\">"
  "        OurGroceries (ourgroceries.com)"
  "    </div>"
  "    "
  ""
  "    <div id=\"addListDialog\" class=\"ui-helper-hidden\">"
  "        <div id=\"addListContainer\">"
  "        <label for=\"addListName\"><span id=\"addDialogLabel\">Shopping list name:</span>&nbsp;</label><input type=\"text\" id=\"addListName\">"
  "        <div id=\"addListErrorMessage\"></div>"
  "        </div>"
  "    </div>"
  ""
  ""
  "        <div style=\"display: none;\">"
  "        <img src=\"/static/images/white_capsule_endcap_left.png\" alt=\"\">"
  "        <img src=\"/static/images/white_capsule_endcap_right.png\" alt=\"\">"
  "    </div>"
  "</div>"
  ""
  "<div id=\"overlayNotice\"></div>"
  ""
  "                    <div id=\"follow-us\">"
  "                        <center>"
  "                            <a href=\"/contact\">Contact us</a> &nbsp;&nbsp;"
  "                            <a href=\"http://www.twitter.com/ourgroceries\" target=\"_blank\"><img src=\"/static/images/twitter@48.png?v=5t867h3h67\" class=\"social-icons\" title=\"Follow us on Twitter\" alt=\"Twitter\"></a> &nbsp;&nbsp;"
  "                            <a href=\"http://www.facebook.com/OurGroceries\" target=\"_blank\"><img src=\"/static/images/facebook@48.png?v=3j543fg569\" class=\"social-icons\" title=\"Like us on Facebook\" alt=\"Facebook\"></a> &nbsp;&nbsp;"
  "                            <a href=\"https://www.zazzle.com/store/ourgroceries\" target=\"_blank\">OurGroceries&nbsp;merch!</a> &nbsp;&nbsp;"
  "                        </center>"
  "                    </div>"
  "                    <table class=\"footer\" cellpadding=\"0\" cellspacing=\"0\">"
  "                        <tr>"
  "                            <td class=\"footer\">"
  "                                <p>Copyright &copy; 2020 HeadCode"
  "                                | <a href=\"/privacy\">Privacy policy</a><br>"
  "                                All rights reserved. HeadCode and OurGroceries are"
  "                                trademarks of HeadCode.</p>"
  "                            </td>"
  "                        </tr>"
  "                    </table>"
  "                </div>"
  "            </div>"
  "    </body>"
  "</html>";
  //cout << response << endl;
  string teamId;
  
  int ret = retrieveTeamId((void *) response, &teamId);

  if (teamId != "cvDwQAKokxat770WwtakIU")
    return 1;

  return 0;
}
