/**
 *    ncgroceries is a CL utility for handling ourgroceries.com shopping lists
 *    Copyright (C) 2019 Henrik Grimler
 *
 *    This file is part of ncgroceries.
 *
 *    ncgroceries is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    ncgroceries is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with ncgroceries. If not, see <https://www.gnu.org/licenses/>.
 */

#include <list>

#include "ourgroceries_API.hpp"

int main()
{
  map<string, info> shoppingList;
  map<string, info>::iterator shopping_it;
  list<string>::iterator on, off;

  char json_list[] = "{\"list\":{\"name\":\"Test List\",\"id\":\"oJ5I9EslHMR852aqMHrdNS\",\"notes\":\"\",\"listType\":\"SHOPPING\",\"externalListAccess\":\"NONE\",\"versionId\":\"9vKEo3JyyyV094AAs3RTjb\",\"items\":[{\"name\":\"1 kg rice\",\"value\":\"1 kg rice\",\"id\":\"Nt73p6xFQtx7s9ZFPF9IN8\"},{\"name\":\"apples\",\"value\":\"apples\",\"id\":\"QuM2Gflu8AmqMDMdOlTVd0\"},{\"name\":\"eggs (2)\",\"value\":\"eggs (2)\",\"id\":\"Q3w6Ua2HYQtE4sqR5QctSI\"},{\"name\":\"2 bananas\",\"value\":\"2 bananas\",\"id\":\"0xrTSgQrUqY5oNCfNSGa5a\",\"crossedOffAt\":1689368379000},{\"name\":\"1 kg äpplen\",\"value\":\"1 kg äpplen\",\"id\":\"qbpWZaG6fnrug63XMPZZOB\",\"crossedOffAt\":1689368380000}]},\"command\":\"getList\"}";

  list<string> items = {"1 kg rice", "apples", "eggs (2)"};
  list<string> crossed_items = {"1 kg äpplen", "2 bananas"};
  istringstream list_stream(json_list);

  int ret = parseList(&list_stream, &shoppingList);
  if (ret != 0) {
    cerr << "Some error occured" << endl;
    return 1;
  }
  on = items.begin();
  off = crossed_items.begin();
  for (shopping_it=shoppingList.begin(); shopping_it!=shoppingList.end(); ++shopping_it) {
    if (!shopping_it->second.crossedOff) {
      if (shopping_it->first != *on) {
        cerr << "Comparison " << shopping_it->first << " = " << *on << " failed" << endl;
        return 1;
      }
      ++on;
    } else {
      if (shopping_it->first != *off) {
        cerr << "Comparison " << shopping_it->first << " = " << *off << " failed" << endl;
        return 1;
      }
      ++off;
    }
  }

  return 0;
}
