/**
 *    ncgroceries is a CL utility for handling ourgroceries.com shopping lists
 *    Copyright (C) 2019 Henrik Grimler
 *
 *    This file is part of ncgroceries.
 *
 *    ncgroceries is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    ncgroceries is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with ncgroceries. If not, see <https://www.gnu.org/licenses/>.
 */

#include "ourgroceries_API.hpp"

int cleanup_and_exit(int exit_code)
{
  curl_global_cleanup();
  exit(exit_code);
}

string trim(string str) {
  size_t first = str.find_first_not_of(' ');
  if (string::npos == first) {
    return str;
  }
  size_t last = str.find_last_not_of(' ');
  return str.substr(first, (last - first + 1));
}

void split(string sss, list<string> *splits)
{
  istringstream ss(sss);
  string split;
  while (getline(ss, split, ',')) {
    (*splits).push_back(trim(split));
  }
}

int parse_arguments(int argc, const char *argv[], bool *ssl, string *email, string *pass,
                    string *list, std::list<string>* items_to_add, std::list<string>* items_to_cross, bool *print_list)
{
  char *verify_ssl = NULL;
  *ssl = true;
  int ch;
  char *emailadress = NULL;
  char *password = NULL;
  char *main_list = NULL;
  char *item_add = NULL;
  char *item_cross = NULL;
  struct poptOption options[] = {
    {"verify-ssl", 'v', POPT_ARG_STRING|POPT_ARGFLAG_OPTIONAL, &verify_ssl, 7,
     "Verify SSL traffic (no allows for mitm inspection)", "yes/no"},
    {"username"  , 'u', POPT_ARG_STRING|POPT_ARGFLAG_OPTIONAL, &emailadress, 6,
     "Email address for loggin into our groceries", "email"},
    {"password"  , 'p', POPT_ARG_STRING|POPT_ARGFLAG_OPTIONAL, &password, 5,
     "Password for logging into our-groceries", "password"},
    {"list"      , 'l', POPT_ARG_STRING|POPT_ARGFLAG_OPTIONAL, &main_list, 4,
     "Name of shopping list to retreive", "list-name"},
    {"add"       , 'a', POPT_ARG_STRING|POPT_ARGFLAG_OPTIONAL, &item_add, 3,
     "Item to add", "item"},
    {"cross"     , 'x', POPT_ARG_STRING|POPT_ARGFLAG_OPTIONAL, &item_cross, 2,
     "Item to cross off", "item"},
    {"print-list", 'L', POPT_ARG_NONE|POPT_ARGFLAG_OPTIONAL, NULL, 1,
     "print content of main list", ""},
    POPT_AUTOHELP
    POPT_TABLEEND
  };

  poptContext pc = poptGetContext(NULL, argc, argv, options, 0);
  while ((ch = poptGetNextOpt(pc)) >= 0) {
    switch (ch) {
    case 1:
      *print_list = true;
      break;
    default:
      break;
    }
  }

  if (emailadress) {
    *email = (string)emailadress;
  } else if ((*email).empty()) {
    cerr << "Error: no username after parsing config files and command line arguments" << endl;
    return EXIT_FAILURE;
  }
  if (password) {
    *pass = (string)password;
  } else if ((*pass).empty()) {
    cerr << "Error: no password after parsing config files and command line arguments" << endl;
    return EXIT_FAILURE;
  }
  if (main_list) {
    *list = (string)main_list;
  } else if ((*list).empty()) {
    /* Default list at our groceries is "Shopping list" so fall back to that if it is not configured */
    *list = "Shopping list";
  }
  if (item_add) {
    if (((string)item_add)[0] == '[' && ((string)item_add)[((string)item_add).size() - 1] == ']') {
      split(((string)item_add).substr(1, ((string)item_add).size() - 2), items_to_add);
    } else {
      (*items_to_add).push_back((string)item_add);
    }
  }
  if (item_cross) {
    if (((string)item_cross)[0] == '[' && ((string)item_cross)[((string)item_cross).size() - 1] == ']') {
      split(((string)item_cross).substr(1, ((string)item_cross).size() - 2), items_to_cross);
    } else {
      (*items_to_cross).push_back((string)item_cross);
    }
  }
  if (verify_ssl)
    if (strcmp(verify_ssl, "no") == 0 || strcmp(verify_ssl, "No") == 0 || strcmp(verify_ssl, "NO") == 0 ||
      strcmp(verify_ssl, "false") == 0 || strcmp(verify_ssl, "False") == 0 || strcmp(verify_ssl, "FALSE") == 0) {
      *ssl = false;
    }

  poptFreeContext(pc);
  return 0;
}

int main(int argc, const char *argv[])
{
  string teamId;
  string email;
  string pass;
  string main_list;
  list<string> items_to_add;
  list<string> items_to_cross;
  bool verify_ssl = true;
  bool print_list = false;
  map<string, string> listArray;
  map<string, info> shoppingList;
  map<string, info>::iterator shopping_it;
  list<string>::iterator list_it;

  curl_global_init(CURL_GLOBAL_ALL);

  if (initialize() != 0) {
    cout << "Seems we ran into an error when initializing ncgroceries" << endl;
    cleanup_and_exit(EXIT_FAILURE);
  }

  read_config(&email, &pass, &main_list);

  if (parse_arguments(argc, argv, &verify_ssl, &email, &pass, &main_list, &items_to_add, &items_to_cross, &print_list) != 0) {
    cleanup_and_exit(EXIT_FAILURE);
  }

  if (doLogin(email, pass, verify_ssl) != 0) {
    cout << "An error occured while logging in to ourgroceries" << endl;
    cleanup_and_exit(EXIT_FAILURE);
  }

  if (getTeamId(&teamId, verify_ssl) != 0) {
    cout << "An error occured while getting teamId" << endl;
    cleanup_and_exit(EXIT_FAILURE);
  }

  if (getOverview(teamId, &listArray, verify_ssl) != 0) {
    cout << "An error occured while getting overview" << endl;
    cleanup_and_exit(EXIT_FAILURE);
  }

  if (listArray.count(main_list) == 1) {
    if (getList(teamId, listArray.find(main_list)->second, &shoppingList, verify_ssl) != 0) {
      cout << "An error occured while getting list" << endl;
      cleanup_and_exit(EXIT_FAILURE);
    }
  } else {
    cout << "Error: " << main_list << " does not seem the name of a shopping list in your account" << endl;
    cleanup_and_exit(EXIT_FAILURE);
  }

  if (!items_to_add.empty()) {
    for (list_it = begin(items_to_add); list_it != end(items_to_add); ++list_it) {
      if (addItem(listArray.find(main_list)->second, teamId, *list_it, &shoppingList, verify_ssl) != 0) {
        cout << "An error occured while inserting item" << endl;
        cleanup_and_exit(EXIT_FAILURE);
      }
    }
  }

  if (!items_to_cross.empty()) {
    for (list_it = begin(items_to_cross); list_it != end(items_to_cross); ++list_it) {
      shopping_it = shoppingList.find(*list_it);
      if (setItemCrossedOff(listArray.find(main_list)->second, teamId,
                            shopping_it, &shoppingList, verify_ssl) != 0) {
        cout << "An error occured while crossing item off" << endl;
        cleanup_and_exit(EXIT_FAILURE);
      }
    }
  }

  if (print_list) {
    cout << "Shopping list '" << main_list << "' contains" << endl;
    list<string> categories;

    for (shopping_it=shoppingList.begin(); shopping_it!=shoppingList.end(); ++shopping_it) {
      if (find(categories.begin(), categories.end(), shopping_it->second.category) == categories.end())
        categories.push_back(shopping_it->second.category);
    }
    for (list_it=categories.begin(); list_it!=categories.end(); ++list_it) {
      for (shopping_it=shoppingList.begin(); shopping_it!=shoppingList.end(); ++shopping_it) {
        if (shopping_it->second.category == *list_it) {
          if (!shopping_it->second.crossedOff) {
            cout << shopping_it->first<< endl;
          }
        }
      }
      cout << " " << endl;
    }
  }
  curl_global_cleanup();
  return 0;
}
