# https://stackoverflow.com/a/18006720
# - Try to find Jsoncpp
# Once done, this will define
#
#  Jsoncpp_FOUND - system has Jsoncpp
#  Jsoncpp_INCLUDE_DIRS - the Jsoncpp include directories
#  Jsoncpp_LIBRARIES - link these to use Jsoncpp

# include(LibFindMacros)

# Use pkg-config to get hints about paths
# libfind_pkg_check_modules(Jsoncpp_PKGCONF jsoncpp)
find_package(PkgConfig QUIET)
if (PKG_CONFIG_FOUND)
  pkg_search_module(Jsoncpp_PKGCONF QUIET jsoncpp)
endif ()

# Include dir
find_path(Jsoncpp_INCLUDE_DIR
  NAMES json/features.h json/json_features.h
  PATH_SUFFIXES jsoncpp
  PATHS ${Jsoncpp_PKGCONF_INCLUDE_DIRS} # /usr/include/jsoncpp/json
)

# Finally the library itself
find_library(Jsoncpp_LIBRARY
  NAMES jsoncpp
  PATHS ${Jsoncpp_PKGCONF_LIBRARY_DIRS}
#  PATH ./jsoncpp/
)

# set(Jsoncpp_PROCESS_INCLUDES Jsoncpp_INCLUDE_DIR)
# set(Jsoncpp_PROCESS_LIBS Jsoncpp_LIBRARY)
# libfind_process(Jsoncpp)

include(FindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(Jsoncpp
  REQUIRED_VARS Jsoncpp_INCLUDE_DIR Jsoncpp_LIBRARY
  VERSION_VAR PC_Jsoncpp_VERSION)

MARK_AS_ADVANCED(Jsoncpp_INCLUDE_DIR Jsoncpp_LIBRARY)
